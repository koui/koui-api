# Koui API

[Code documentation]() for the [Koui](https://gitlab.com/koui/Koui) GUI package.

## Building:

- Install [Git](https://git-scm.com/downloads), [Haxe](https://haxe.org/) and [dox](https://lib.haxe.org/p/dox) and make sure you can run `git` and `haxe` from the command line

- Create a copy or add a symlink to [Kha](https://github.com/Kode/Kha) in the parent folder of your local Koui repository

- Open a terminal in that parent folder and run the following command:
  
  ```bash
  git clone https://gitlab.com/koui/koui-api.git
  ```
  
  > If you have configured SSH, you can also clone this repository by executing the following:
  > 
  > ```bash
  > git clone git@gitlab.com:koui/koui-api.git
  > ```

- Now, the folder structure should look as follows:
  
  ```
  Parent Directory/
  │
  ├── Kha/
  ├── Koui/
  └── koui-api/
  ```

- Then, from the terminal, run `cd koui-api` and then `make`

- The generated documentation files can be found in the `koui-api/pages` directory

## Troubleshooting

- Make sure your `Kha` and `Koui` folders are named exactly so, `koui` instead of `Koui` (e.g.) will not work!

- If you encounter other problems, please open an [issue](https://gitlab.com/MoritzBrueckner/koui-api/issues/new). If you are able to fix it yourself, please open a [Merge Request](https://gitlab.com/MoritzBrueckner/koui-api/-/merge_requests). Thank you :)
