rmdir /S /Q pages
copy /Y "..\Koui\Assets\theme.ksn" /B "krom\theme.ksn" /B

call haxe api.hxml -xml build/api.xml -D doc-gen
if not %errorlevel% == 0 (
	echo Haxe compiler exited with error code %errorlevel%, aborting...
	pause
	exit /b %errorlevel%
) else (
	haxelib run dox -i build --toplevel-package koui --title "Koui API" -theme ./theme -D logo "https://koui.gitlab.io/api/koui_icon.png" -D website "https://koui.gitlab.io" -D source-path "https://gitlab.com/koui/Koui/-/tree/master/Sources"
)

pause
