#!/bin/bash

function pause() {
    if [ "$1" != "--ci" ]
    then
	   read -n 1 -s -r -p "Press any key to continue"
    fi
}

rm -rf pages
mkdir -p "krom"
cp "../Koui/Assets/theme.ksn" "krom/theme.ksn"

haxe api.hxml -xml build/api.xml -D doc-gen
status=$?
if [ $status -ne 0 ]
then
	echo "Haxe compiler exited with error code $status, aborting..."
	pause "$1"
	exit $status
else
	haxelib run dox -i build --toplevel-package koui --title "Koui API" -theme ./theme -D logo "https://koui.gitlab.io/api/koui_icon.png" -D website "https://koui.gitlab.io" -D source-path "https://gitlab.com/koui/Koui/-/tree/master/Sources"
fi

pause "$1"
